package com.intellectualapps.app.utils;

import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;


import org.parceler.guava.primitives.Doubles;

import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {
    private static final String EDIT_TEXT_ERROR_TEXT = "Please fill this field";
    private static final String EDIT_TEXT_PHONE_ERROR_TEXT = "Please provide a valid phone number";
    private static final String EDIT_TEXT_AMOUNT_ERROR_TEXT = "Please provide a valid amount";
    private static final String EDIT_TEXT_LENGTH = "Please obey the size rule!";
    private static final String INVALID_PASSWORD_ERROR = "Please make sure your passwords are the same";
    private static final String EDIT_TEXT_EMAIL_ADDRESS_TEXT = "Please provide a valid email address";
    private static final String EDIT_TEXT_MAX_AMOUNT_ERROR_TEXT = "Please provide a value less than or equal to ";
    private static final String EDIT_TEXT_LENGTH_ERROR = "Must not be less than % digits!";
    private static final String PASSWORD_VALIDATION_ERROR_MESSAGE = "Invalid password";

    public Validator() {
    }

    public static boolean validateInputNotEmpty(EditText editText) {
        if (editText == null) {
            return false;
        }

        if (isInputEmpty(editText)) {
            showError(editText, EDIT_TEXT_ERROR_TEXT, true, true);
            return false;
        } else {
            showError(editText, null, false, true);
            return true;
        }
    }

    public static boolean validateInputViewsNotEmpty(EditText[] editTextViews) {
        for (EditText editText : editTextViews) {
            if (editText == null) {
                return false;
            } else {
                if (isInputEmpty(editText)) {
                    showError(editText, EDIT_TEXT_ERROR_TEXT, true, true);
                    return false;
                } else {
                    showError(editText, null, false, true);
                }
            }
        }
        return true;
    }

    public static boolean validatePasswordInput(EditText editText) {
        if (editText == null) {
            return false;
        } else {
            if (isInputEmpty(editText)) {
                showError(editText, EDIT_TEXT_ERROR_TEXT, true, true);
                return false;
            } else {
                String password = editText.getText().toString().trim();
                if (password.length() == 0) {
                    showError(editText, null, false, true);
                    return false;
                }

                boolean passwordValidationFlag = isValidPassword(password);

                if (!passwordValidationFlag) {
                    showError(editText, PASSWORD_VALIDATION_ERROR_MESSAGE, true, true);
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean validatePhoneNumber(EditText editText) {
        if (editText == null) {
            return false;
        } else {
            if (isInputEmpty(editText)) {
                showError(editText, EDIT_TEXT_ERROR_TEXT, true, true);
                return false;
            } else {
                if (isValidPhoneNumber(editText)) {
                    showError(editText, null, false, true);
                    return true;
                } else {
                    showError(editText, EDIT_TEXT_PHONE_ERROR_TEXT, true, true);
                    return false;
                }
            }
        }
    }

    public static boolean validateInputLength(EditText editText, int length) {
        if (editText == null) {
            return false;
        } else {
            if (isInputEmpty(editText)) {
                showError(editText, EDIT_TEXT_ERROR_TEXT, true, true);
                return false;
            } else {
                if (isValidLength(editText, length)) {
                    showError(editText, null, false, true);
                } else {
                    showError(editText, EDIT_TEXT_LENGTH_ERROR.replace("%", String.valueOf(length)), true, true);
                    return false;
                }
            }
        }

        return true;
    }


    public static boolean validateEmailAddress(EditText editText) {
        if (editText == null) {
            return false;
        } else {
            if (isInputEmpty(editText)) {
                showError(editText, EDIT_TEXT_ERROR_TEXT, true, true);
                return false;
            } else {
                boolean flag = Patterns.EMAIL_ADDRESS.matcher(editText.getText().toString().trim()).matches();

                if (!flag) {
                    showError(editText, EDIT_TEXT_EMAIL_ADDRESS_TEXT, true, true);
                } else {
                    showError(editText, null, false, true);
                }
                return flag;
            }
        }
    }

    public static boolean validateExpiryDateInput(EditText editText, String errorMessage) {
        if (editText == null) {
            return false;
        } else {
            if (isInputEmpty(editText)) {
                showError(editText, EDIT_TEXT_ERROR_TEXT, true, true);
                return false;
            } else {
                String text = editText.getText().toString();
                boolean flag = isValidDate(text);

                if (!flag) {
                    showError(editText, errorMessage, true, true);
                }
                return flag;
            }
        }
    }

    public static boolean validateAmountInput(EditText editText) {
        if (editText == null) {
            return false;
        } else {
            if (isInputEmpty(editText)) {
                showError(editText, EDIT_TEXT_ERROR_TEXT, true, true);
                return false;
            } else {
                String text = editText.getText().toString();
                text = text.replaceAll(",", "");
                if (Doubles.tryParse(text) != null) {
                    Double amount = Doubles.tryParse(text);
                    boolean flag = amount != null && amount > 0.0;

                    if (!flag) {
                        showError(editText, EDIT_TEXT_AMOUNT_ERROR_TEXT, true, true);
                    }
                    return flag;
                } else {
                    showError(editText, EDIT_TEXT_AMOUNT_ERROR_TEXT, true, true);
                    return false;
                }
            }
        }
    }

    public static boolean validateAmountInputAgainstValue(EditText editText, Double value) {
        if (editText == null) {
            return false;
        } else {
            if (isInputEmpty(editText)) {
                showError(editText, EDIT_TEXT_ERROR_TEXT, true, true);
                return false;
            } else {
                String text = editText.getText().toString();
                text = text.replaceAll(",", "");
                if (Doubles.tryParse(text) != null) {
                    Double amount = Doubles.tryParse(text);
                    amount = amount != null && amount > 0 ? amount : 0.0;
                    boolean flag = amount > 0.0;
                    boolean isInputBelowMaxValue = amount <= value;

                    if (!flag) {
                        showError(editText, EDIT_TEXT_AMOUNT_ERROR_TEXT, true, true);
                    }

                    if (!isInputBelowMaxValue) {
                        showError(editText, EDIT_TEXT_MAX_AMOUNT_ERROR_TEXT + CommonUtils.formatAmountToCurrencyWithDecimalPlaces(value), true, true);
                    }

                    return flag && isInputBelowMaxValue;
                } else {
                    showError(editText, EDIT_TEXT_AMOUNT_ERROR_TEXT, true, true);
                    return false;
                }
            }
        }
    }

    public static boolean validatePasswordsEqual(EditText passwordInput, EditText confirmPasswordInput) {
        if (passwordInput == null || confirmPasswordInput == null) {
            return false;
        }

        if (isInputEmpty(passwordInput) || isInputEmpty(confirmPasswordInput)) {
            passwordInput.setError(INVALID_PASSWORD_ERROR);
            confirmPasswordInput.setError(INVALID_PASSWORD_ERROR);
            return false;
        }

        if (passwordInput.getText().toString().trim().equalsIgnoreCase(confirmPasswordInput.getText().toString().trim())) {
            passwordInput.setError(null);
            confirmPasswordInput.setError(null);
            return true;
        } else {
            confirmPasswordInput.setError(INVALID_PASSWORD_ERROR);
            confirmPasswordInput.requestFocus();
            return false;
        }
    }


     /*
    **
     *  Helpers
    **
    */

    public static void focusView(View view) {
        view.requestFocus();
        view.getParent().requestChildFocus(view, view);
    }

    public static void showError(EditText editText, String message, boolean enabled, boolean isTextInputErrorDisabled) {
        if (!isTextInputErrorDisabled) {
            TextInputLayout textInputLayout = (TextInputLayout) editText.getRootView().findViewById(editText.getId()).getParent().getParent();
            textInputLayout.setErrorEnabled(enabled);

            if (enabled) {
                focusView(editText);
            }
            textInputLayout.setError(message);
            dismissErrorHandler(editText, textInputLayout);
        } else {
            if (enabled) {
                editText.setError(message);
                focusView(editText);
            }
        }
    }

    private static void dismissErrorHandler(EditText editText, final TextInputLayout textInputLayout) {
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    textInputLayout.setError(null);
                    textInputLayout.setErrorEnabled(false);

                }
            }
        });
        editText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textInputLayout.setError(null);
                textInputLayout.setErrorEnabled(false);
            }
        });
    }

    public static boolean isValidPassword(final String password) {
        Pattern pattern;
        Matcher matcher;

        String PASSWORD_PATTERN = Constants.PASSWORD_VALIDATION_REGEX;

        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();
    }

    private static boolean isInputEmpty(EditText editText) {
        if (editText == null) {
            return true;
        }

        String inputValue = editText.getText().toString().trim();

        return TextUtils.isEmpty(inputValue);
    }

    private static boolean isValidPhoneNumber(EditText editText) {
        if (editText == null) {
            return false;
        }

        String inputValue = editText.getText().toString().trim();
        return inputValue.length() == 11;
    }

    private static boolean isValidLength(EditText editText, int maxLength) {
        if (editText == null) {
            return false;
        }

        int inputValue = editText.getText().length();
        return inputValue == maxLength;
    }

    private static boolean isValidDate(String expiryDate) {
        if (!TextUtils.isEmpty(expiryDate) && expiryDate.length() == 5) {
            String month = expiryDate.substring(0, 2);
            String year = expiryDate.substring(3, 5);

            int monthpart = -1, yearpart = -1;

            try {
                monthpart = Integer.parseInt(month) - 1;
                yearpart = Integer.parseInt(year);

                Calendar current = Calendar.getInstance();
                current.set(Calendar.DATE, 1);
                current.set(Calendar.HOUR, 12);
                current.set(Calendar.MINUTE, 0);
                current.set(Calendar.SECOND, 0);
                current.set(Calendar.MILLISECOND, 0);

                Calendar validity = Calendar.getInstance();
                validity.set(Calendar.DATE, 1);
                validity.set(Calendar.HOUR, 12);
                validity.set(Calendar.MINUTE, 0);
                validity.set(Calendar.SECOND, 0);
                validity.set(Calendar.MILLISECOND, 0);

                if (monthpart > -1 && monthpart < 12 && yearpart > -1) {
                    validity.set(Calendar.MONTH, monthpart);
                    validity.set(Calendar.YEAR, yearpart + 2000);
                } else
                    return false;

                Log.d("Util", "isValidDate: " + current.compareTo(validity));

                if (current.compareTo(validity) <= 0)
                    return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return false;
    }

}