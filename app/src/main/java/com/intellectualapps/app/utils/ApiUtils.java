package com.intellectualapps.app.utils;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.intellectualapps.app.CustomApplication;
import com.intellectualapps.app.R;
import com.intellectualapps.app.data.api.ApiModule;
import com.intellectualapps.app.data.api.responses.DefaultResponse;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okio.Buffer;
import retrofit2.Converter;
import retrofit2.Response;

public class ApiUtils {
    private static final String HEADER_PLATFORM = "X-Platform";
    private static final String HEADER_DEVICE_ID = "X-Native-Device-Id";
    private static final String HEADER_USER_LATITUDE = "X-Coord-Lat";
    private static final String HEADER_USER_LONGITUDE = "X-Coord-Lon";
    private static final String HEADER_AUTHORIZATION = "Authorization";
    private static final String HEADER_APP_VERSION = "X-App-Version";

    private static final String APP_PLATFORM = "1";

    public static Map<String, String> buildHeaders(Map<String, String> extraHeaders) {
        Map<String, String> headers = new HashMap<String, String>();

        Context context = CustomApplication.getAppInstance().getApplicationContext();

        headers.put(HEADER_DEVICE_ID, StringUtils.nullify(PreferenceStorageManager.getDeviceId(context)));
        headers.put(HEADER_PLATFORM, APP_PLATFORM);
        headers.put(HEADER_APP_VERSION, StringUtils.nullify(AppUtils.getAppVersion(context)));
        headers.put(HEADER_AUTHORIZATION, "Bearer: " + StringUtils.nullify(PreferenceStorageManager.getAuthToken(context)));

        if (extraHeaders != null)
            headers.putAll(extraHeaders);

        return headers;
    }

    public static RequestBody getRequestBody(String value) {
        RequestBody requestBody = RequestBody.create(MediaType.parse("text/plain"), value);
        return requestBody;
    }


    public static String bodyToString(final RequestBody request) {
        try {
            final RequestBody copy = request;
            final Buffer buffer = new Buffer();
            copy.writeTo(buffer);
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "";
        } catch (Exception e) {
            return "";
        }
    }

    private static void updateAuthenticationToken(DefaultResponse response) {

    }

    public static boolean isSuccessResponse(Response<? extends DefaultResponse> response) {
        String error = CustomApplication.getApplicationResources().getString(R.string.response_failure_message);
        if (response != null) {
            if (response.isSuccessful()) {
                if (response.body() != null && !TextUtils.isEmpty(response.body().getAuthToken())) {
                    updateAuthenticationToken(response.body());
                }
                return true;
            } else {
                Converter<ResponseBody, DefaultResponse> converter =
                        ApiModule.buildRetrofitAdapter(CustomApplication.getCacheFile(), null)
                                .responseBodyConverter(DefaultResponse.class, new Annotation[0]);
                DefaultResponse defaultResponse = null;
                try {
                    if (response.errorBody() != null) {
                        defaultResponse = converter.convert(response.errorBody());
                    }
                    CommonUtils.displayShortToastMessage((defaultResponse != null && defaultResponse.getMessage() != null ? (String) defaultResponse.getMessage() : error));
                } catch (Exception e) {
                    CommonUtils.displayShortToastMessage(error);
                }
            }
        } else {
            CommonUtils.displayShortToastMessage(error);
        }

        return false;
    }
}
